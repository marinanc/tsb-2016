
import junit.framework.TestSuite;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class UtilsTest extends TestSuite {
    
    @Before
    public void setup(){
        
    }
    
    @Test
    public void testSumar() {
        int a = 2;
        int b = 3;
        int suma = Utils.sumar(a, b);
        Assert.assertEquals("No son iguales", 5, suma);
    }

    @Test
    public void testRestar() {
        int a = 3;
        int b = 2;
        int suma = Utils.restar(a, b);
        Assert.assertEquals("No son iguales", 1, suma);
    }
    
}
