
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class ListaTest {
    
    private Lista l;
    
    @Before
    public void setup() {
        l = new Lista();
    }
    
    @Test
    public void testSizeVacio() {
        assertEquals(0, l.size());
    }
    @Test
    public void testIsEmpty() {
        assertTrue(l.isEmpty());
    }
    
    @Test
    public void testIsNotEmpty() {
        l.insertar(1);
        assertFalse(l.isEmpty());
    }
    
    @Test
    public void testInsertar() {
        l.insertar(1);
        assertEquals(1, l.size());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testInsertarNoAceptaNegativos() {
        l.insertar(-1);
    }
    
    @Test
    public void testToString() {
        assertEquals("{}", l.toString());
    }

    @Test
    public void testToStringUnNodo() {
        l.insertar(1);
        assertEquals("{1}", l.toString());
    }
    
    @Test
    public void testToStringDosNodos() {
        l.insertar(1);
        l.insertar(2);
        assertEquals("{2, 1}", l.toString());
    }
}
