/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utn.tsb.ordenamiento;

/**
 *
 * @author gfbett
 */
public class Ordenamiento {
    
    public static void selectionSort(int [] v) {
        int n = v.length;
        for(int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (v[i] > v[j]) {
                    int aux = v[i];
                    v[i] = v[j];
                    v[j] = aux;
                }
            }
        }
    }
    
    public static void bubbleSort(int[] v) {
        int n = v.length;
      
        boolean ordenado = false;
        for (int i = 0; i < n-1 && !ordenado; i++) {
            ordenado = true;
            for (int j = 0; j < n-i-1; j++) {
                if (v[j] > v[j+1]) {
                    int aux = v[j];
                    v[j] = v[j+1];
                    v[j+1] = aux;
                    ordenado = false;
                }
            }
        }
    }
    
    public static void insertionSort(int[] v) {
        int n = v.length;
        for (int i = 1; i < n; i++) {
            int j, valor = v[i];
            for (j = i -1; j >= 0 && v[j] > valor; j-- ) {
                v[j + 1] = v[j];
            }
            v[j+1] = valor;
        }
    }
    
    // ordenamiento Quick Sort
    public static void quickSort(int[] v) {
        ordenar(v, 0, v.length - 1);
    }

    private static void ordenar(int[] v, int izq, int der) {
        int i = izq, j = der, y;
        int x = v[(izq + der) / 2];
        do {
            while (v[i] < x && i < der) {
                i++;
            }
            while (x < v[j] && j > izq) {
                j--;
            }
            if (i <= j) {
                y = v[i];
                v[i] = v[j];
                v[j] = y;
                i++;
                j--;
            }
        } while (i <= j);
        if (izq < j) {
            ordenar(v, izq, j);
        }
        if (i < der) {
            ordenar(v, i, der);
        }
    }
    
    public static void shellSort(int[] v) {
        int h, n = v.length;
        for (h = 1; h <= n / 9; h = 3 * h + 1);
        for (; h > 0; h /= 3) {
            for (int j = h; j < n; j++) {
                int k, y = v[j];
                for (k = j - h; k >= 0 && y < v[k]; k -= h) {
                    v[k + h] = v[k];
                }
                v[k + h] = y;
            }
        }
    }
    
    // ordenamiento Heap Sort
    public static void heapSort(int[] v) {
        int n = v.length;

        // crear el grupo inicial...
        for (int i = 1; i < n; i++) {
            int e = v[i];
            int s = i;
            int f = (s - 1) / 2;
            while (s > 0 && v[f] < e) {
                v[s] = v[f];
                s = f;
                f = (s - 1) / 2;
            }
            v[s] = e;
        }

        // se extrae la raiz, y se reordena el vector y el grupo...
        for (int i = n - 1; i > 0; i--) {
            int valori = v[i];
            v[i] = v[0];
            int f = 0, s;
            if (i == 1) {
                s = -1;
            } else {
                s = 1;
            }
            if (i > 2 && v[2] > v[1]) {
                s = 2;
            }
            while (s >= 0 && valori < v[s]) {
                v[f] = v[s];
                f = s;
                s = 2 * f + 1;
                if (s + 1 <= i - 1 && v[s] < v[s + 1]) {
                    s++;
                }
                if (s > i - 1) {
                    s = -1;
                }
            }
            v[f] = valori;
        }
    }

}
  