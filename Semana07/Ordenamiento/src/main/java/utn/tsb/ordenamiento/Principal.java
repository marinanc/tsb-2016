/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utn.tsb.ordenamiento;

/**
 *
 * @author gfbett
 */
public class Principal {
    public static void main(String[] args) {
        int[] v = new int[100000];
        for(int i = 0; i < v.length; i++) {
            v[i] = (int)(Math.random() * v.length);
            //v[i] = i;
            //v[i] = v.length - i;
        }
        int[] x = new int[v.length];
        System.out.println("Burbuja");
        System.arraycopy(v, 0, x, 0, v.length);
        long before = System.currentTimeMillis();
        Ordenamiento.bubbleSort(x);
        long after = System.currentTimeMillis();
        long tiempo = after - before;
        System.out.println("Tiempo: " + tiempo + "ms");

        System.out.println("Selección Directa");
        System.arraycopy(v, 0, x, 0, v.length);
        before = System.currentTimeMillis();
        Ordenamiento.selectionSort(x);
        after = System.currentTimeMillis();
        tiempo = after - before;
        System.out.println("Tiempo: " + tiempo + "ms");

        System.out.println("InsertionSort");
        System.arraycopy(v, 0, x, 0, v.length);
        //printArray(x);
        before = System.currentTimeMillis();
        Ordenamiento.insertionSort(x);
        after = System.currentTimeMillis();
        tiempo = after - before;
        //printArray(x);
        System.out.println("Tiempo: " + tiempo + "ms");

        System.out.println("QuickSort");
        System.arraycopy(v, 0, x, 0, v.length);
        //printArray(x);
        before = System.currentTimeMillis();
        Ordenamiento.quickSort(x);
        after = System.currentTimeMillis();
        tiempo = after - before;
        //printArray(x);
        System.out.println("Tiempo: " + tiempo + "ms");


        System.out.println("ShellSort");
        System.arraycopy(v, 0, x, 0, v.length);
        //printArray(x);
        before = System.currentTimeMillis();
        Ordenamiento.shellSort(x);
        after = System.currentTimeMillis();
        tiempo = after - before;
        //printArray(x);
        System.out.println("Tiempo: " + tiempo + "ms");        
        
        System.out.println("HeapSort");
        System.arraycopy(v, 0, x, 0, v.length);
        //printArray(x);
        before = System.currentTimeMillis();
        Ordenamiento.heapSort(x);
        after = System.currentTimeMillis();
        tiempo = after - before;
        //printArray(x);
        System.out.println("Tiempo: " + tiempo + "ms");
    }

    private static void printArray(int[] v) {

        System.out.println("\n");

        for(int i = 0; i < 20; i ++) {
            System.out.print(" - " + v[i]);
        }
        
        System.out.println("\n");
    }
}
