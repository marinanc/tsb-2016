/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Recursion {
    public static void main(String[] args) {
        long x = factorial(5);
        System.out.println(x);
    }
    
    private static int factorial(int x) {
        if (x == 0) {
            return 1;
        }
        return x * factorial(x - 1);
    }

}
