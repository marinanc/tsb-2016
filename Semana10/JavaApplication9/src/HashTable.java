/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class HashTable {
    
    private int size;
    private Entry[] values;
    
    
    public HashTable() {
        size = 0;
        values = new Entry[100];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void put(int key, int value) {
        int index = hash(key);
        Entry newEntry = new Entry(key, value);
        boolean found = false;
        while(!found) {
            if (values[index] == null) {
                size++;
                values[index] = newEntry;
                found = true;
            } else {
                if (values[index].key == key) {
                    values[index].value = value;
                    found = true;
                } else {
                    index ++;
                }
            }
            if (index == values.length) {
                index = 0;
            }
        }
    }

    public int get(int key) {
        int index = hash(key);
        boolean found = false;
        while (!found) {
            if (values[index] == null) {
               found = true; 
            } else if (values[index].key == key) {
                return values[index].value;
            } else {
                index++;
            }
            if (index == values.length) {
                index = 0;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public int size() {
        return size;
    }

    private int hash(int key) {
        return key % values.length;
    }

    private static class Entry {
        int key;
        int value;

        public Entry(int key, int value) {
            this.key = key;
            this.value = value;
        }

    }
    
}
