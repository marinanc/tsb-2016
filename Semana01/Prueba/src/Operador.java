/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Operador extends Nodo {
    private char operador;
    private Nodo izquierdo;
    private Nodo derecho;

    public Operador(char operador, Nodo izquierdo, Nodo derecho) {
        this.operador = operador;
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }

    @Override
    public int valuar() {
        switch(operador) {
            case '*':
                    return izquierdo.valuar() * derecho.valuar();
            case '-':
                    return izquierdo.valuar() - derecho.valuar();
            case '+':
                    return izquierdo.valuar() + derecho.valuar();
            case '/':
                    return izquierdo.valuar() / derecho.valuar();
        }
        return -1;
    }
    
}


