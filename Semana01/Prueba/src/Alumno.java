public class Alumno extends Persona{
    
    private int legajo;
    
    public Alumno() {
        this(3);
    }

    public Alumno(int dni) {
        super(dni, "Unknown");
    }

    public int getLegajo() {
        return legajo;
    }
    
    @Override
    public String toString() {
        return "Alumno: " + super.toString();
    }

    @Override
    public void hortaliza() {
        System.out.println("Hortaliza2 ");
    }
    
    
}
