package tsb;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Iterator;

public class Lista<T> implements Iterable<T> {
    private Nodo<T> frente;
    private int size;

    public Lista() {
        this.size = 0;
    }

    public void insertar(T x) {
        Nodo<T> nuevo = new Nodo<>(x);
        nuevo.setNext(frente);
        frente = nuevo;
        size++;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Nodo<T> p = frente;
        while (p != null) {
            sb.append(p.getInfo());
            sb.append(", ");
            p = p.getNext();
        }
        sb.append("}");
        return sb.toString();
    }

    public int size() {
        return this.size;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of bounds");
        }
        Nodo<T> p = frente;
        for (int i = 0; i < index; i++) {
            p = p.getNext();
        }
        return p.getInfo();
    }

    @Override
    public Iterator<T> iterator() {
        return new MiIterador();
    }

    private class MiIterador implements Iterator<T> {
        private Nodo<T> actual;

        MiIterador() {
            actual = frente;
        }

        @Override
        public boolean hasNext() {
            return actual != null;
        }

        @Override
        public T next() {
            T valor = actual.getInfo();
            actual = actual.getNext();
            return valor;
        }

        @Override
        public void remove() {
            //TODO: Implement
            throw new NotImplementedException();
        }
    }

}