
import tsb.listas.Lista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Test {
    
    public static void main(String[] args) {
        
        final int longitud = 10000;
        Lista l = new Lista();
        for(int i = 0;i < longitud; i++) {
            l.insertar(i);
        }
        long antes = System.currentTimeMillis();
        int suma = 0;
        for (int i = 0; i < longitud; i++) {
            suma += (Integer)l.get(i);
        }
        long despues = System.currentTimeMillis();
        System.out.println("La suma es:" + suma);
        long tiempo = despues-antes;
        System.out.println("Tiempo transcurrido: " + tiempo);
        
        
    }
    
}
